# Python3 - TDD com Python

### Conceitos abordados:
- Testes;
- Porque usar testes;
- Vantagens do uso de testes;
- Testes no Python;
- Padrão de nomenclaturas dos testes em Python;
- Auxilio do PyCharm na criação dos testes;
- Biblioteca unittest;
- Isolamento de cenário com o método setUp;
- Cópia rasa de lista;
- Programação defensiva;
- Encapsulamento e comportamento;
- Um pouco sobre modelagem de classes;
- TDD (desenvolvimento orientado a testes);
- Refatoração;
- Gerenciador de contextos __with__;
- pytest;
- Refatoração;
- Exceções;
- Criar exceções.

![TDD](/uploads/24c9d6063a8df1ed0a617640f26c0450/TDD.gif)